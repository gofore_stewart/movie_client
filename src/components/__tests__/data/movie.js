export const MOVIE = {
  name: "Avengers: Endgame",
  year: 2018,
  genres: ["Adventure", "Sci-fi"],
  ageLimit: 12,
  rating: 5,
  actors: [
    {
      firstName: "Robert",
      lastName: " Downey Jr.",
    },
    {
      firstName: "Chris",
      lastName: "Evans",
    },
    {
      firstName: "Scarlett",
      lastName: "Johansson",
    },
  ],
  director: {
    firstName: "Anthony",
    lastName: "Russo",
  },
  synopsis:
    "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
};
