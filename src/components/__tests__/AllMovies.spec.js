import { describe, it, expect, vi } from "vitest";
import { mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";

import AllMovies from "../../modules/movie/components/AllMovies.vue";
import { useMovieStore } from "../../stores/movie.js";
import MovieUI from "../../modules/movie/components/MovieUI.vue";
import { MOVIE } from "./data/movie.js";

describe("AllMovies", () => {
  it("renders properly", async () => {
    const wrapper = mount(AllMovies, {
      shallow: true,
      global: {
        plugins: [
          createTestingPinia({
            createSpy: vi.fn,
            initialState: {
              movie: { allMovies: [MOVIE] },
            },
          }),
        ],
      },
    });
    const store = useMovieStore();
    expect(store.allMovies).toEqual([MOVIE]);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.findComponent(MovieUI).exists()).toBe(true);
    expect(wrapper.findComponent(MovieUI).props("movie")).toEqual(MOVIE);
  });
});
