export const BASE_URL = "http://localhost:8080/movies";

export const GENRES = [
  "Comedy",
  "Fantasy",
  "Crime",
  "Drama",
  "Music",
  "Adventure",
  "History",
  "Thriller",
  "Animation",
  "Family",
  "Mystery",
  "Biography",
  "Action",
  "Film-Noir",
  "Romance",
  "Sci-Fi",
  "War",
  "Western",
  "Horror",
  "Musical",
  "Sport",
];

export const PAGE = {
  allMovies: "allMovies",
  searchResult: "searchResult",
};
