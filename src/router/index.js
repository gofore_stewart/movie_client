import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: import.meta.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      // route level code-splitting
      // this generates a separate chunk (IndexView.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/IndexView.vue"),
    },
    {
      path: "/item",
      name: "item",
      component: () => import("../views/ItemView.vue"),
    },
    {
      path: "/create",
      name: "create",
      component: () => import("../views/FormView.vue"),
    },
    {
      path: "/edit",
      name: "edit",
      component: () => import("../views/FormView.vue"),
    },
  ],
});

export default router;
