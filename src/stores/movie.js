import { defineStore } from "pinia";
import axios from "axios";
import { errorToast, successToast } from "../utility/toaster.js";
import { PAGE, BASE_URL } from "../modules/movie/constants/";

// Store for managing movies and movies search results
export const useMovieStore = defineStore("movie", {
  state: () => ({
    allMovies: [], // list of all movies
    searchQuery: "", // current search query
    placeHolder: "", // message to display in absence of movie eg error or no movies
    searchResult: [], // list of movies in search results
    isFetchingMovies: false, // checks if all movie request is actively executing
    isFetchingSearch: false, // checks if search movie request is actively executing
    activeComponent: PAGE.allMovies, // active component either allMovies or searchResults
    selected: null, // selected movie item
  }),
  getters: {
    findAllMovies(state) {
      return state.allMovies;
    },
    findSearchResults(state) {
      return state.searchResult;
    },
    findActiveComponent(state) {
      return state.activeComponent;
    },
    getSelected(state) {
      return state.selected;
    },
  },
  actions: {
    // fetch all movies
    async onFetch() {
      try {
        this.isFetchingMovies = true;
        this.placeHolder = "";
        const response = await axios.get(BASE_URL);
        this.allMovies = response.data;
        this.placeHolder = response.data.length === 0 ? "no movie found" : "";
        this.isFetchingMovies = false;
      } catch (error) {
        errorToast(error);
        this.placeHolder = error;
        this.isFetchingMovies = false;
      }
    },
    // delete a movie
    async onDelete(id) {
      try {
        await axios.delete(BASE_URL + "/" + id);
        successToast("movie deleted successfully");
      } catch (error) {
        errorToast(error);
      }
    },
    // search for movie
    async onSearch(query) {
      try {
        this.isFetchingSearch = true;
        this.placeHolder = "";
        const response = await axios.get(BASE_URL + "?search=" + query);
        this.searchResult = response.data;
        this.placeHolder =
          response.data.length === 0 ? "no movie found for " + query : "";
        this.isFetchingSearch = false;
      } catch (error) {
        errorToast(error);
        this.placeHolder = error;
        this.isFetchingSearch = false;
      }
    },
  },
});
