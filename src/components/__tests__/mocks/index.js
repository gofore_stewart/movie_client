import { MOVIE } from "../data/movie.js";

export const MovieUI = {
  name: "MovieUI",
  props: [{ movie: MOVIE }],
  template: `<div>{{ movie.title }}</div>`,
};
