import { defineStore } from "pinia";
import axios from "axios";
import _ from "lodash";
import { errorToast, successToast } from "../utility/toaster.js";
import { BASE_URL } from "../modules/movie/constants/";

// Store for managing movie form state and data
export const useMovieFormStore = defineStore({
  id: "movieForm",
  state: () => ({
    step: 1, // current form step
    totalSteps: 1, // total form steps/page
    isUpdate: false, // form loading type either create or update
    obj: null, // movie object currently in the form
    genres: [], // list of genres
    actors: [], // list of actors
  }),
  actions: {
    setObj(obj) {
      this.obj = Object.assign({}, this.obj, obj);
    },
    removeActors(value) {
      this.actors = _.filter(this.actors, function (e) {
        return e.firstName !== value.firstName && e.lastName !== value.lastName;
      });
    },
    addActors(obj) {
      this.actors.push(obj);
    },
    // create new movie
    async onCreate(obj) {
      try {
        await axios.post(BASE_URL, obj);
        successToast("movie was created successfully");
      } catch (error) {
        errorToast(error);
      }
    },
    // update movie
    async onEdit(obj) {
      try {
        const id = obj._id;
        delete obj._id;
        await axios.patch(BASE_URL + "/" + id, obj);
        successToast("movie is updated successfully");
      } catch (error) {
        errorToast(error);
      }
    },
  },
});
