import { describe, it, expect } from "vitest";

import { mount } from "@vue/test-utils";
import FormHeader from "../../modules/movie/components/FormHeader.vue";
const DATA = { title: "TEST", step: 1, total: 5 };
describe("FormHeader", () => {
  it("renders properly", () => {
    const wrapper = mount(FormHeader, {
      propsData: { data: DATA },
    });
    expect(wrapper.props().data).toBe(DATA);
  });
});
