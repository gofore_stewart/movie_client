import { describe, it, expect, vi } from "vitest";
import { PiniaVuePlugin } from "pinia";
import { createLocalVue, mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";
import Buefy from "buefy";
import MovieItem from "../../modules/movie/components/MovieItem.vue";
import { useMovieStore } from "../../stores/movie.js";
import MovieUI from "../../modules/movie/components/MovieUI.vue";
import DeleteModal from "../../components/gadgets/DeleteModal.vue";
import { MOVIE } from "./data/movie.js";
const localVue = createLocalVue();
localVue.use(PiniaVuePlugin);
localVue.use(Buefy);

describe("MovieItem", () => {
  it("renders properly", async () => {
    const wrapper = mount(MovieItem, {
      shallow: false,
      stubs: {
        BModal: true,
      },
      data() {
        return {
          isDeleteActive: false,
        };
      },
      localVue,
      pinia: createTestingPinia({
        createSpy: vi.fn,
        initialState: {
          movie: { selected: MOVIE }, // start the counter at 20 instead of 0
        },
      }),
    });
    const store = useMovieStore();
    expect(store.selected).toBe(MOVIE);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.vm.isDeleteActive).toBe(false);

    await wrapper.find("button[name='delete']").trigger("click");
    expect(wrapper.findComponent(DeleteModal).exists()).toBe(true);
    expect(wrapper.findComponent(MovieUI).exists()).toBe(true);
    expect(wrapper.findComponent(MovieUI).props("movie")).toEqual(MOVIE);
  });
});
