/**
 * Form input validators
 */
import { extend } from "vee-validate";
import { required, confirmed, integer } from "vee-validate/dist/rules";

/**
 * maxLines validates maximum number of lines in the text
 * @param {number} max maximum number of lines
 */
extend("maxLines", {
  validate(value, { max }) {
    const newLines = value.split("\n").length;
    return newLines <= max;
  },
  params: ["max"],
  message: "{_field_} field requires maximum {max} lines",
});

// validate the input is required
extend("required", {
  ...required,
  message: "{_field_} field is required",
});

extend("integer", {
  ...integer,
  message: "{_field_} field is required",
});

// validate two inputs are similar
extend("confirmed", {
  ...confirmed,
  message: "{_field_} does not match original field",
});

// validate positive number
extend("positive", (value) => {
  return value > -1;
});

/**
 * minValue validates minimum length of the text
 * @param {number} min minimum value/number of a number
 */
extend("minValue", {
  validate(value, { min }) {
    return value >= min;
  },
  params: ["min"],
  message: "{_field_} minimum is {min}",
});

/**
 * maxValue validates maximum value of a number
 * @param {number} max maximum value/number of a number
 */
extend("maxValue", {
  validate(value, { max }) {
    return value <= max;
  },
  params: ["max"],
  message: "{_field_} maximum is {max}",
});

/**
 * minLength validates maximum length of the text
 * @param {number} length minimum length of text or array
 */
extend("minLength", {
  validate(value, { length }) {
    return value.length >= length;
  },
  params: ["length"],
  message: "{_field_} requires minimum {length} characters",
});

/**
 * maxLength validates maximum length of the text
 * @param {number} length maximum length of text or array
 */
extend("maxLength", {
  validate(value, { length }) {
    return value.length <= length;
  },
  params: ["length"],
  message: "{_field_} requires maximum {length} characters",
});

extend("isValidUrl", {
  validate(value) {
    try {
      return !!new URL(value);
    } catch (_) {
      return false;
    }
  },
  message: "{_field_} is not valid url",
});

extend("checkProperty", {
  validate(value, { fields }) {
    const result = {
      isValid: true,
      label: "",
    };
    for (const key in value) {
      if (value[key].length === 0) {
        result.isValid = false;
        result.label = key;
        break;
      }
    }
    return result.isValid;
  },
  params: ["fields"],
  message: "{_field_} field requires valid {fields}",
});

extend("isInteger", {
  validate(value) {
    return Number.isInteger(value);
  },
  message: "{_field_} requires valid integer",
});

extend("isYear", {
  validate(value, { min, max }) {
    return (value - min) * (value - max) <= 0;
  },
  params: ["min", "max"],
  message: "{_field_} requires valid year",
});
