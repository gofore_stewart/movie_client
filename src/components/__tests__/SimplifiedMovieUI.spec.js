import { describe, it, expect } from "vitest";

import { mount } from "@vue/test-utils";
import SimplifiedMovieUI from "../../modules/movie/components/SimplifiedMovieUI.vue";
import { MOVIE } from "./data/movie.js";

describe("SimplifiedMovieUI", () => {
  it("renders properly", () => {
    const wrapper = mount(SimplifiedMovieUI, {
      propsData: { movie: MOVIE },
    });
    expect(wrapper.props().movie).toBe(MOVIE);
  });
});
