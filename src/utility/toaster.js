import { ToastProgrammatic as Toast } from "buefy";

/*
display error toast
 */
export function errorToast(err) {
  Toast.open({
    message: err.message,
    position: "is-top",
    type: "is-danger",
    duration: 6000,
  });
}

/*
display success toast
 */
export function successToast(message) {
  Toast.open({
    message: message,
    position: "is-top",
    type: "is-success",
    duration: 6000,
  });
}
